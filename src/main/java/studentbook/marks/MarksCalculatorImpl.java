package studentbook.marks;

import studentbook.model.MarksData;

public class MarksCalculatorImpl implements MarksCalculator {
    @Override
    public int calculateSumMarks(MarksData marksData) {
        int sum = getSum(marksData);
        return sum;
    }

    @Override
    public float calculateAvgMarks(MarksData marksData) {
        float avg = (float) getSum(marksData) / marksData.getMarksSubjects().size();
        return avg;
    }

    @Override
    public String resultStatus(MarksData marksData) {
        int totalFail = 0;
        for (Integer i : marksData.getMarksSubjects()) {
            if (i < 20) {
                totalFail++;
            }
        }
        return (totalFail <= 0) ? "All Clear" : "Total Fail Subjects: " + totalFail;
    }

    private static int getSum(MarksData marksData) {
        int sum = 0;
        for (Integer i : marksData.getMarksSubjects()) {
            sum = sum + i;
        }
        return sum;
    }
}
