package studentbook.marks;

import studentbook.model.MarksData;

public interface MarksCalculator {
    int calculateSumMarks(MarksData marksData);
    float calculateAvgMarks(MarksData marksData);
    String resultStatus(MarksData marksData);
}
