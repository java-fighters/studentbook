package studentbook.model;

import java.util.List;

public class MarksData {
    private List<Integer> MarksSubjects;

    public List<Integer> getMarksSubjects() {
        return MarksSubjects;
    }

    public void setMarksSubjects(List<Integer> marksSubjects) {
        MarksSubjects = marksSubjects;
    }

    @Override
    public String toString() {
        return "MarksData{" +
                "MarksSubjects=" + MarksSubjects +
                '}';
    }
}
