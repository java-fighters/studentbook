package studentbook.model;

import java.util.List;

public class StudentDisplay {
    private String fullName;
    private int age;
    private long mobileNo;
    private String gender;
    private List<String> mails;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(long mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getMails() {
        return mails;
    }

    public void setMails(List<String> mails) {
        this.mails = mails;
    }

    @Override
    public String toString() {
        String newLine = "\n";
        return "StudentDisplay{" +
                "\nfullName='" + fullName + '\'' + newLine +
                "age=" + age + newLine +
                "mobileNo=" + mobileNo + newLine +
                "gender='" + gender + '\'' + newLine +
                "mails=" + mails + newLine +
                '}';
    }
}
