package studentbook.registration;

import studentbook.model.StudentDisplay;
import studentbook.model.StudentInformation;

public interface RegisterNewStudent {
    StudentDisplay registerNewStudent(StudentInformation studentInformation);
    void displayData(StudentDisplay studentInformation);
}
