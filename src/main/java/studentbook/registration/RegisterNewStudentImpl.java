package studentbook.registration;

import studentbook.model.StudentDisplay;
import studentbook.model.StudentInformation;

import java.time.LocalDate;
import java.time.Period;

public class RegisterNewStudentImpl implements RegisterNewStudent {

    @Override
    public StudentDisplay registerNewStudent(StudentInformation studInfo) {
        //processed data
        String fullName = studInfo.getFirstName().concat(" " + studInfo.getLastName());
        int finalAge = calculateAge(studInfo.getDob());

        //creating obj
        StudentDisplay studentDisplay = new StudentDisplay();
        studentDisplay.setFullName(fullName);
        studentDisplay.setAge(finalAge);
        studentDisplay.setMobileNo(studInfo.getMobileNumber());
        studentDisplay.setGender(studInfo.getGender());
        studentDisplay.setMails(studInfo.getMails());

        return studentDisplay;

    }

    @Override
    public void displayData(StudentDisplay studentInformation) {
        System.out.println(studentInformation.toString());
    }

    private int calculateAge(String birthDate) {
        // Parse the user input into a LocalDate object
        LocalDate dob = LocalDate.parse(birthDate);
        // Calculate age
        LocalDate currentDate = LocalDate.now();
        Period period = Period.between(dob, currentDate);
        int age = period.getYears();
        return age;
    }
}
