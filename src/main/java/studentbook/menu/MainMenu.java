package studentbook.menu;

import studentbook.marks.MarksCalculatorImpl;
import studentbook.model.MarksData;
import studentbook.model.StudentDisplay;
import studentbook.model.StudentInformation;
import studentbook.registration.RegisterNewStudentImpl;

import java.util.Scanner;

import static studentbook.data.SettingData.getStudentInformation;
import static studentbook.data.SettingData.marksSetter;
import static studentbook.util.Constants.*;
import static studentbook.util.Constants.CALCULATOR;

public class MainMenu {

    public static int getChoice() {
        int choice = 0;
        System.out.println("Menu: " + NEW_LINE + REGISTER_NEW_USER + CALCULATOR + EXIT);
        try {
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            mainMenuMethod(choice);
            return choice;
        } catch (Exception e) {
            System.out.println("Only Numeric Input Is Allowed");
        }

        return choice;
    }

    private static void mainMenuMethod(int choice) {
        switch (choice) {
            case 1:
                registration();
                break;
            case 2:
                calculations();
                break;
            case 3:
                System.out.println(EXIT.substring(2));
                break;
            default:
                System.out.println("Please Enter Choice Within 1..3");
                break;
        }

    }

    private static void calculations() {
        //marks calculator
        MarksData marksData = new MarksData();
        marksSetter(marksData);

        MarksCalculatorImpl mc = new MarksCalculatorImpl();
        System.out.println("Sum Of Marks: " + mc.calculateSumMarks(marksData));
        System.out.println("Avg Of Marks: " + mc.calculateAvgMarks(marksData));
        System.out.println(mc.resultStatus(marksData));
    }

    private static void registration() {
        //registration
        StudentInformation information = getStudentInformation();

        RegisterNewStudentImpl rns = new RegisterNewStudentImpl();
        StudentDisplay studentDisplay = rns.registerNewStudent(information);
        rns.displayData(studentDisplay);
    }
}
