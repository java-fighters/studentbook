package studentbook.util;

public class Constants {
    public static final String REGISTER_NEW_USER = "1. Add New User\n";
    public static final String CALCULATOR = "2. Calculate Data\n";
    public static final String EXIT = "3. Exit\n";
    public static final String NEW_LINE = "\n";

}
