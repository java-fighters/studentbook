package studentbook.data;

import studentbook.model.MarksData;
import studentbook.model.StudentInformation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SettingData {
    public static void marksSetter(MarksData marksData) {
        List<Integer> marks = new ArrayList<>();
        marks.add(19);
        marks.add(10);
        marks.add(40);
        marks.add(35);
        marks.add(45);
        marksData.setMarksSubjects(marks);
    }

    public static StudentInformation getStudentInformation() {
        StudentInformation information = new StudentInformation();
        information.setFirstName("Shailendra");
        information.setLastName("Patil");
        information.setDob("1990-05-15");
        information.setGender("Mail");
        information.setMobileNumber(9146277038L);
        List<String> mails = new LinkedList<>();
        mails.add("shailendradilippatil@gmail.com");
        mails.add("fakemail@techyatri.com");
        mails.add("vishalpatil@gmail.com");
        information.setMails(mails);
        return information;
    }
}
